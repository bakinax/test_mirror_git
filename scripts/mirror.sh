#!/bin/bash
SRC_REPO="https://gitlab.com/bakinax"
DEST_REPO="http://lyovgitlabmui.lyon-dev2.local/cbalisky"
GROUP_NAME="cbalisky"
PROJECT_NAME="test_git_mirror"
REPOSITORY_PATH="/home/users/projects"

cd $REPOSITORY_PATH
if [ ! -d $PROJECT_NAME.git ]; then
  git clone --mirror $SRC_REPO/$PROJECT_NAME.git
fi
cd $PROJECT_NAME.git
git remote set-url --push origin $DEST_REPO/$PROJECT_NAME
git fetch -p origin
git push --mirror